#include "knownrule.h"

KnownRule::KnownRule() {}

KnownRule::KnownRule(std::vector<double> inputs, double output):
    in(inputs), out(output) {}

const std::vector<double> &KnownRule::inputs() const { return in; }

double KnownRule::output() const { return out; }
