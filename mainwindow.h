#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "som.h"

#include <vector>

#include <QColor>
#include <QDoubleSpinBox>
#include <QGridLayout>
#include <QMainWindow>
#include <QPushButton>
#include <QSpinBox>
#include <QTimer>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void visitMapItem(size_t row, size_t col, size_t classVal);
    void drawMap();
private slots:
    void train();
    void pause();
    void reset();
    void handleTrainingIter();
private:
    struct MapDataItem
    {
        int red;
        int green;
        int blue;
        std::vector<size_t> visitors;
        std::vector<size_t> visits;
    };
    using MapDataRow = std::vector<MapDataItem>;

    void setupGUI();
    std::vector<KnownRule> datasetFromIDX(const QString &imagesPath, const QString &labelsPath,
                                          size_t takeN);
    void generateRandomDatasets();
    void clearMapData();
    void showMapResults(const std::vector<KnownRule> &testSet);

    Ui::MainWindow *ui;
    QLabel *m_currentItersLabel;
    QSpinBox *m_itersSpin;
    QDoubleSpinBox *m_learnSpin;
    QPushButton *m_trainBtn;
    QPushButton *m_pauseBtn;
    QPushButton *m_resetBtn;
    QGridLayout *m_map;
    size_t m_mapRows;
    size_t m_mapCols;
    std::vector<MapDataRow> m_mapData;
    const std::vector<QColor> m_colors = {
        QColor(230, 25, 75),    // Red
        QColor(60, 180, 75),    // Green
        QColor(255, 225, 25),   // Yellow
        QColor(245, 130, 48),   // Orange
        QColor(70, 240, 240),   // Cyan
        QColor(240, 50, 230),   // Magenta
        QColor(170, 110, 40),   // Brown
        QColor(128, 0, 0),      // Maroon
        QColor(0, 0, 128),      // Navy
        QColor(0, 0, 0)         // Black
    };
    SOM *m_som;
    QTimer m_clock;
    int m_clockTimer;
    std::vector<KnownRule> m_mnistTrainSet;
    std::vector<KnownRule> m_mnistFastTestSet;
    std::vector<KnownRule> m_mnistPreciseTestSet;
    const int MNIST_IMAGES_FILE_MAGIC_NUMBER = 0x0803;
    const int MNIST_LABELS_FILE_MAGIC_NUMBER = 0x0801;
    const size_t MNIST_PIXEL_COUNT = 28 * 28;
    const size_t MNIST_TRAIN_SET_SIZE_OF_EACH_CLASS = 100;
    const size_t MNIST_FAST_TEST_SIZE_OF_EACH_CLASS = 1;
    const size_t MNIST_PRECISE_TEST_SIZE_OF_EACH_CLASS = 500;
};

#endif // MAINWINDOW_H
