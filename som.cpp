#include "som.h"

#include <algorithm>
#include <random>

#include <QDebug>

SOM::SOM(size_t sideLength,
         size_t dimension,
         const std::tuple<double, double> &initWeightsRange,
         std::vector<KnownRule> rules,
         size_t maxIters,
         size_t neighborhoodSize,
         double initialLearningRate) :
    m_rules(rules),
    m_sideLength(sideLength),
    m_maxIters(maxIters),
    m_itersRun(0),
    m_learningRate(initialLearningRate),
    m_neighborhodSize(neighborhoodSize)
{
    for (size_t i = 0; i < sideLength; ++i)
    {
        std::vector<Neuron> newLayer;
        for (size_t j = 0; j < sideLength; ++j)
        {
            newLayer.push_back(Neuron(dimension, initWeightsRange));
        }
        m_neurons.push_back(newLayer);
    }
}

size_t SOM::itersRun() const
{
    return m_itersRun;
}

SOM::State SOM::runTrainingIter()
{
    if (m_state == Stopped) return m_state;

    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(m_rules.begin(), m_rules.end(), g);
    for (auto rule : m_rules)
    {
        auto bmu = findBMU(rule.inputs());
        updateWeights(bmu, rule.inputs());
    }

    m_state = ++m_itersRun == m_maxIters ? Stopped : Training;
    return m_state;
}

std::pair<size_t, size_t> SOM::findBMU(std::vector<double> input)
{
    std::pair<size_t, size_t> bmu;
    double minDst = -1;
    for (size_t i = 0; i < m_sideLength; ++i)
    {
        for (size_t j = 0; j < m_sideLength; ++j)
        {
            double currentDst = vectorDistance(input, m_neurons[i][j].weights());
            if (currentDst < minDst || minDst < 0)
            {
                bmu.first = i;
                bmu.second = j;
                minDst = currentDst;
            }
        }
    }
    return bmu;
}

void SOM::updateWeights(std::pair<size_t, size_t> bmu, std::vector<double> input)
{
    auto neighborhood = getNeighborNeurons(bmu);
    for (auto &neuron : neighborhood)
    {
        auto deltas = vectorDifference(input, neuron->weights());
        for (size_t i = 0; i < deltas.size(); i++)
        {
            neuron->weights()[i] += m_learningRate * deltas[i];
        }
    }
}

double SOM::vectorDistance(std::vector<double> x, std::vector<double> y)
{
    return vectorNorm(vectorDifference(x, y));
}

std::vector<double> SOM::vectorDifference(std::vector<double> x, std::vector<double> y)
{
    std::vector<double> result;
    for (size_t i = 0; i < x.size(); i++)
    {
        result.push_back(x[i] - y[i]);
    }
    return result;
}

double SOM::vectorNorm(std::vector<double> x)
{
    double result = 0;
    for (auto d : x)
    {
        result += d * d;
    }
    return sqrt(result);
}

std::vector<Neuron*> SOM::getNeighborNeurons(std::pair<size_t, size_t> center)
{
    std::vector<Neuron*> result;
    result.push_back(&m_neurons[center.first][center.second]);
    if (center.first > 0)
    {
        result.push_back(&m_neurons[center.first - 1][center.second]);
    }
    if (center.first + 1 < m_neurons.size())
    {
        result.push_back(&m_neurons[center.first + 1][center.second]);
    }
    if (center.second > 0)
    {
        result.push_back(&m_neurons[center.first][center.second - 1]);
    }
    if (center.second + 1 < m_neurons.size())
    {
        result.push_back(&m_neurons[center.first][center.second + 1]);
    }
    return result;
}
