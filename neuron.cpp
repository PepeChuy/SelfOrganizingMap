#include "neuron.h"

#include <random>       /* std::random_device, std::mt19937,
                           std::uniform_real_distribution */
#include <algorithm>    // std::generate

Neuron::Neuron(size_t dimension,
              std::tuple<double, double> initSearchRange) :
    m_weights(dimension)
{
    // Initialize state
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> randDistrib(
        std::get<0>(initSearchRange), std::get<1>(initSearchRange)
    );
    std::generate(m_weights.begin(), m_weights.end(),
        [&]() -> double { return randDistrib(gen); }
    );
}
