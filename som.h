#ifndef SOM_H
#define SOM_H

#include <vector>
#include <cmath>

#include "knownrule.h"
#include "neuron.h"


class SOM
{
    public:
        enum State
        {
            New,
            Training,
            Stopped
        };

        SOM(size_t sideLength,
            size_t dimension,
            const std::tuple<double, double> &initWeightsRange,
            std::vector<KnownRule> rules,
            size_t maxIters,
            size_t neighborhoodSize,
            double initialLearningRate);

        size_t itersRun() const;

        State runTrainingIter();

        std::pair<size_t, size_t> findBMU(std::vector<double> input);

    private:
        std::vector<KnownRule> m_rules;
        std::vector<std::vector<Neuron>> m_neurons;
        size_t m_sideLength;
        size_t m_maxIters;
        size_t m_itersRun;
        double m_learningRate;
        size_t m_neighborhodSize;
        State m_state;

        void updateWeights(std::pair<size_t, size_t> bmu, std::vector<double> input);

        double vectorDistance(std::vector<double> x, std::vector<double> y);
        std::vector<double> vectorDifference(std::vector<double> x, std::vector<double> y);
        double vectorNorm(std::vector<double> x);
        std::vector<Neuron*> getNeighborNeurons(std::pair<size_t, size_t> center);

};

#endif // SOM_H
