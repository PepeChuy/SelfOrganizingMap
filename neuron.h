#ifndef NEURON_H
#define NEURON_H

#include "knownrule.h"

#include <vector>
#include <tuple>

class Neuron
{
    public:

        Neuron(size_t dimension,
                std::tuple<double, double> initSearchRange);

        std::vector<double> &weights() { return m_weights; }

    private:
        std::vector<double> m_weights;
};

#endif // NEURON_H
