#ifndef KNOWNRULE_H
#define KNOWNRULE_H

#include <vector>

class KnownRule
{
    public:
        KnownRule();
        KnownRule(std::vector<double> inputs, double output);
        const std::vector<double> &inputs() const;
        double output() const;


        std::vector<double> &inputs() { return in; }
        double &output() { return out; }
    private:
        std::vector<double> in;
        double out;
};

#endif // KNOWNRULE_H
