#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "knownrule.h"
#include "som.h"

#include <algorithm>
#include <random>

#include <QDataStream>
#include <QDebug>
#include <QFile>
#include <QFormLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QIODevice>
#include <QLabel>
#include <QSizePolicy>
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_mapRows(15),
    m_mapCols(15),
    m_mapData(std::vector<MapDataRow>(m_mapRows, MapDataRow(m_mapCols, MapDataItem()))),
    m_som(nullptr),
    m_clockTimer(100)
{
    ui->setupUi(this);
    m_currentItersLabel = new QLabel("0");
    m_itersSpin = new QSpinBox();
    m_learnSpin = new QDoubleSpinBox();
    m_trainBtn = new QPushButton("Train");
    m_pauseBtn = new QPushButton("Pause");
    m_resetBtn = new QPushButton("Reset");
    connect(m_trainBtn, &QPushButton::clicked, this, &MainWindow::train);
    connect(m_pauseBtn, &QPushButton::clicked, this, &MainWindow::pause);
    connect(m_resetBtn, &QPushButton::clicked, this, &MainWindow::reset);
    connect(&m_clock, &QTimer::timeout, this, &MainWindow::handleTrainingIter);
    setupGUI();
    generateRandomDatasets();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_som;
}

void MainWindow::visitMapItem(size_t row, size_t col, size_t classVal)
{
    auto &data = m_mapData[row][col];
    bool found = false;
    size_t classIdx = 0;
    size_t maxVisits = 0;
    for (size_t i = 0; i < data.visitors.size(); ++i)
    {
        if (!found && data.visitors[i] == classIdx)
        {
            found = true;
            classIdx = i;
        }
        if (data.visits[i] > maxVisits) maxVisits = data.visits[i];
    }
    if (!found)
    {
        classIdx = data.visitors.size();
        data.visitors.push_back(classVal);
        data.visits.push_back(0);
    }
    if (++data.visits[classIdx] >= maxVisits)
    {
        auto &color = m_colors[classVal];
        data.red = color.red();
        data.green = color.green();
        data.blue = color.blue();
    }
}

void MainWindow::drawMap()
{
    size_t maxN = 0;
    for (auto &row : m_mapData)
    {
        for (auto &item : row)
        {
            for (auto &visits : item.visits)
            {
                if (visits > maxN)
                {
                    maxN = visits;
                }
            }
        }
    }
    for (size_t i = 0; i < m_mapRows; ++i)
    {
        for (size_t j = 0; j < m_mapCols; ++j)
        {
            size_t maxVisits = 0;
            const auto &data = m_mapData[i][j];
            for (auto &visits : data.visits)
            {
                if (visits > maxVisits)
                {
                    maxVisits = visits;
                }
            }
            auto styleStr = QString("background-color: rgba(%1, %2, %3, %4);")
                            .arg(data.red)
                            .arg(data.green)
                            .arg(data.blue)
                            .arg(data.visitors.size() > 0 ? 1 : 0);
            m_map->itemAtPosition(static_cast<int>(i), static_cast<int>(j))->widget()
                 ->setStyleSheet(styleStr);
        }
    }
}

void MainWindow::train()
{
    m_trainBtn->setText("Train");
    m_trainBtn->setEnabled(false);
    m_pauseBtn->setEnabled(true);
    m_resetBtn->setEnabled(true);
    auto maxIters = static_cast<size_t>(m_itersSpin->value());
    auto learnRate = m_learnSpin->value();
    m_som = new SOM(
        m_mapRows, MNIST_PIXEL_COUNT, {0.0, 255.0}, m_mnistTrainSet, maxIters, 1, learnRate
    );
    m_clock.start(m_clockTimer);
}

void MainWindow::pause()
{
    m_clock.stop();
    m_trainBtn->setText("Continue");
    m_trainBtn->setEnabled(true);
    m_pauseBtn->setEnabled(false);
    m_resetBtn->setEnabled(true);
}

void MainWindow::reset()
{
    if (m_clock.isActive()) m_clock.stop();
    m_currentItersLabel->setText("0");
    m_trainBtn->setText("Train");
    m_trainBtn->setEnabled(true);
    m_pauseBtn->setEnabled(false);
    m_resetBtn->setEnabled(false);
    clearMapData();
    generateRandomDatasets();
    delete m_som;
    m_som = nullptr;
    drawMap();
}

void MainWindow::handleTrainingIter()
{
    auto res = m_som->runTrainingIter();
    m_currentItersLabel->setText(QString::number(m_som->itersRun()));
    if (res == SOM::Training)
    {
        showMapResults(m_mnistFastTestSet);
    }
    else
    {
        m_clock.stop();
        showMapResults(m_mnistPreciseTestSet);
        m_pauseBtn->setEnabled(false);
    }
}

void MainWindow::generateRandomDatasets()
{
    m_mnistTrainSet = datasetFromIDX(":/MNIST/train-images", ":/MNIST/train-labels",
                                     MNIST_TRAIN_SET_SIZE_OF_EACH_CLASS);
    m_mnistFastTestSet = datasetFromIDX(":/MNIST/test-images", ":/MNIST/test-labels",
                                        MNIST_FAST_TEST_SIZE_OF_EACH_CLASS);
    m_mnistPreciseTestSet = datasetFromIDX(":/MNIST/test-images", ":/MNIST/test-labels",
                                           MNIST_PRECISE_TEST_SIZE_OF_EACH_CLASS);
}

void MainWindow::clearMapData()
{
    for (auto &row : m_mapData)
    {
        for (auto &item : row)
        {
            item.red = 0;
            item.green = 0;
            item.blue = 0;
            item.visitors.clear();
            item.visits.clear();
        }
    }
}

void MainWindow::showMapResults(const std::vector<KnownRule> &testSet)
{
    clearMapData();
    for (auto &rule : testSet)
    {
        auto bmu = m_som->findBMU(rule.inputs());
        visitMapItem(bmu.first, bmu.second, static_cast<size_t>(rule.output()));
    }
    drawMap();
}

void MainWindow::setupGUI()
{
    auto panelLayout = new QVBoxLayout();
    auto configGroup = new QGroupBox("Configuration");
    auto configForm = new QFormLayout();
    auto colorsGroup = new QGroupBox("Colors");
    auto colorsGrid = new QGridLayout();
    auto btnsLayout = new QHBoxLayout();
    // SOM Panel: config
    m_itersSpin->setRange(1, 10000);
    m_itersSpin->setSingleStep(100);
    m_itersSpin->setValue(1000);
    m_itersSpin->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));
    m_learnSpin->setRange(0.05, 1.00);
    m_learnSpin->setSingleStep(0.05);
    m_learnSpin->setValue(0.50);
    m_learnSpin->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));
    configForm->addRow(new QLabel("# Iterations"), m_itersSpin);
    configForm->addRow(new QLabel("Learning rate"), m_learnSpin);
    configForm->addRow(new QLabel("Current Iterations"), m_currentItersLabel);
    configGroup->setLayout(configForm);
    // SOM Panel: colors
    size_t colorsGridRowSize = m_colors.size() / 5;
    for (size_t i = 0; i < m_colors.size(); ++i)
    {
        int gridRow = static_cast<int>(i % colorsGridRowSize);
        int gridCol = static_cast<int>(i / colorsGridRowSize) * 2;
        auto styleStr = QString("background-color: rgb(%1, %2, %3);")
                        .arg(m_colors[i].red())
                        .arg(m_colors[i].green())
                        .arg(m_colors[i].blue());
        auto colorBox = new QLabel("");
        colorBox->setFixedSize(15, 15);
        colorBox->setStyleSheet(styleStr);
        colorsGrid->addWidget(colorBox, gridRow, gridCol);
        colorsGrid->addWidget(new QLabel(QString::number(i)), gridRow, gridCol + 1);
    }
    colorsGroup->setLayout(colorsGrid);
    // SOM Panel: buttons
    m_pauseBtn->setEnabled(false);
    m_resetBtn->setEnabled(false);
    btnsLayout->addWidget(m_pauseBtn);
    btnsLayout->addWidget(m_resetBtn);
    // SOM Panel: main
    panelLayout->addWidget(configGroup);
    panelLayout->addWidget(colorsGroup);
    panelLayout->addWidget(m_trainBtn);
    panelLayout->addLayout(btnsLayout);
    // SOM Map
    m_map = new QGridLayout();
    m_map->setSpacing(1);
    for (size_t i = 0; i < m_mapRows; ++i)
    {
        for (size_t j = 0; j < m_mapCols; ++j)
        {
            auto label = new QLabel("");
            label->setFixedSize(15, 15);
            m_map->addWidget(label, static_cast<int>(i), static_cast<int>(j));
        }
    }
    // Main layout
    auto mainLayout = new QHBoxLayout();
    mainLayout->addLayout(m_map);
    mainLayout->addLayout(panelLayout);
    mainLayout->setStretch(0, 5);
    mainLayout->setStretch(1, 1);
    centralWidget()->setLayout(mainLayout);
}

std::vector<KnownRule> MainWindow::datasetFromIDX(const QString &imagesPath,
                                                  const QString &labelsPath,
                                                  size_t takeN)
{
    std::vector<std::vector<KnownRule>> rules(10, std::vector<KnownRule>());
    QFile imagesFile(imagesPath);
    QFile labelsFile(labelsPath);
    QDataStream images(&imagesFile);
    QDataStream labels(&labelsFile);
    int imagesMagicNumber, imagesLen, labelsMagicNumber, labelsLen, pixelCount;
    unsigned char pixel, label;
    images.setByteOrder(QDataStream::ByteOrder::BigEndian);
    labels.setByteOrder(QDataStream::ByteOrder::BigEndian);
    imagesFile.open(QIODevice::ReadOnly);
    labelsFile.open(QIODevice::ReadOnly);
    images >> imagesMagicNumber >> imagesLen >> pixelCount >> pixelCount;
    pixelCount *= pixelCount;
    if (imagesMagicNumber != MNIST_IMAGES_FILE_MAGIC_NUMBER ||
        static_cast<size_t>(pixelCount) != MNIST_PIXEL_COUNT)
    {
        qDebug() << "The MNIST " << imagesPath << " resource is corrupt!";
        exit(1);
    }
    labels >> labelsMagicNumber >> labelsLen;
    if (labelsMagicNumber != MNIST_LABELS_FILE_MAGIC_NUMBER)
    {
        qDebug() << "The MNIST " << labelsPath << " resource is corrupt!";
        exit(1);
    }
    if (imagesLen != labelsLen)
    {
        qDebug() << "Number of images (" << imagesLen << ") does not match number of labels ("
                 << labelsLen << ")!\n Resources: " << imagesPath << " and " << labelsPath;
    }
    for (auto i = 0; i < imagesLen; ++i)
    {
        std::vector<double> ruleInputs;
        for (auto j = 0; j < pixelCount; ++j)
        {
            images >> pixel;
            ruleInputs.push_back(static_cast<double>(pixel));
        }
        labels >> label;
        rules[label].push_back(KnownRule(ruleInputs, static_cast<double>(label)));
    }
    /* Shuffles rules[i] and chooses MNIST_TAKE_OF_EACH_CLASS elements,
       joining all classes and returning the resulting set */
    std::vector<KnownRule> result;
    result.reserve(rules.size() * takeN);
    std::random_device rd;
    std::mt19937 g(rd());
    for (auto &ruleGroup : rules)
    {
        std::shuffle(ruleGroup.begin(), ruleGroup.end(), g);
        ruleGroup.resize(takeN);
        result.insert(result.end(), ruleGroup.begin(), ruleGroup.end());
    }
    std::shuffle(result.begin(), result.end(), g);
    return result;
}
